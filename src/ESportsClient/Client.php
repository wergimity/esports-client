<?php

namespace ESportsClient;

use ESportsClient\Result\Player;
use ESportsClient\Result\PlayerStatistics;
use ESportsClient\Result\Team;
use ESportsClient\Result\Tournament;
use ESportsClient\Result\TournamentStatistics;
use GuzzleHttp\Client as Guzzle;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;
use RuntimeException;
use stdClass;

/**
 * Class Client
 * @package ESportsClient
 */
class Client
{
    /**
     * @var Guzzle
     */
    protected $client;

    /**
     * Client constructor.
     * @param string $baseURI
     * @param array  $credentials
     */
    public function __construct(Guzzle $client = null)
    {
        $this->client = $client;
    }

    /**
     * @param Guzzle $client
     */
    public function setClient(Guzzle $client)
    {
        $this->client = $client;
    }

    /**
     * @return Guzzle
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param int $id
     *
     * @return Tournament|null
     */
    public function tournament(int $id)
    {
        return $this->resource('tournament/' . $id, Tournament::class);
    }

    /**
     * @param int $page
     * @param int $limit
     *
     * @return Tournament[]
     */
    public function tournamentList($page = 1, $limit = 10)
    {
        return $this->collection('tournaments', Tournament::class, compact('page', 'limit'));
    }

    /**
     * @param int $id
     *
     * @return Team
     */
    public function team(int $id)
    {
        return $this->resource('team/' . $id, Team::class);
    }

    /**
     * @param int $page
     * @param int $limit
     *
     * @return Team[]
     */
    public function teamList($page = 1, $limit = 10)
    {
        return $this->collection('teams', Team::class, compact('page', 'limit'));
    }

    /**
     * @param int $tournamentId
     * @param int $page
     * @param int $limit
     *
     * @return Team[]
     */
    public function tournamentTeamList(int $tournamentId, $page = 1, $limit = 10)
    {
        return $this->collection('tournament/' . $tournamentId . '/teams', Team::class, compact('page', 'limit'));
    }

    /**
     * @param int $id
     *
     * @return Player
     */
    public function player(int $id)
    {
        return $this->resource('player/' . $id, Player::class);
    }

    /**
     * @param int $page
     * @param int $limit
     *
     * @return Player[]
     */
    public function playerList($page = 1, $limit = 10)
    {
        return $this->collection('players', Player::class, compact('page', 'limit'));
    }

    /**
     * @param int $teamId
     * @param int $page
     * @param int $limit
     *
     * @return Player[]
     */
    public function teamPlayerList(int $teamId, $page = 1, $limit = 10)
    {
        return $this->collection('team/' . $teamId . '/players', Player::class, compact('page', 'limit'));
    }

    /**
     * @param int $tournamentId
     * @param int $page
     * @param int $limit
     *
     * @return TournamentStatistics[]
     */
    public function tournamentStatisticsList(int $tournamentId, $page = 1, $limit = 10)
    {
        return $this->collection(
            'tournament/' . $tournamentId . '/statistics',
            TournamentStatistics::class,
            compact('page', 'limit')
        );
    }

    /**
     * @param int $tournamentId
     * @param int $playerId
     *
     * @return PlayerStatistics[]
     */
    public function tournamentPlayerStatistics(int $tournamentId, int $playerId)
    {
        return $this->resource(
            'tournament/' . $tournamentId . '/player/' . $playerId . '/statistics',
            PlayerStatistics::class
        );
    }

    /**
     * @param string $uri
     * @param string $resultClass
     *
     * @return mixed
     */
    protected function resource($uri, $resultClass)
    {
        $response = $this->request($uri);

        if ($response->getStatusCode() !== 200) {
            return null;
        }

        $model = json_decode($response->getBody()->getContents());

        return new $resultClass($model);
    }

    /**
     * @param string $uri
     * @param string $resultClass
     * @param array  $query
     *
     * @return array
     */
    protected function collection($uri, $resultClass, $query = [])
    {
        $mapper = function ($item) use ($resultClass) {
            return new $resultClass($item);
        };

        $response = $this->request($uri, $query);

        if ($response->getStatusCode() !== 200) {
            return [];
        }

        return array_map($mapper, json_decode($response->getBody()->getContents()));
    }

    /**
     * @param string $uri
     * @param array  $query
     *
     * @return ResponseInterface
     * @throws RuntimeException
     * @throws ClientException
     * @throws ServerException
     */
    protected function request($uri, $query = [])
    {
        if ($this->client === null) {
            throw new RuntimeException('No guzzle client specified!');
        }

        return $this->client->get($uri, compact('query'));
    }
}