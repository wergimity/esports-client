<?php

namespace ESportsClient;

use Illuminate\Support\ServiceProvider;

class EsportsServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(Client::class, Client::class, true);
    }

    public function boot(Client $client)
    {
        $this->publishes([__DIR__ . '/../config.php' => config_path('esports.php')]);

        $client->setClient($this->buildGuzzleClient());
    }

    /**
     * @return \GuzzleHttp\Client
     */
    private function buildGuzzleClient()
    {
        $base = config('esports.base_uri');
        $credentials = config('esports.credentials');

        $config = $config = [];
        $config['base_uri'] = $base;
        $config['http_errors'] = config('esports.http_errors', false);
        if ($credentials) {
            $config['auth'] = $credentials['username'] . ':' . $credentials['password'];
        }

        return new \GuzzleHttp\Client($config);
    }
}