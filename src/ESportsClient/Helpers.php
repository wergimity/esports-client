<?php

namespace ESportsClient;

use Carbon\Carbon;
use DateTime;

/**
 * Class Helpers
 * @package ESportsClient
 */
abstract class Helpers
{
    /**
     * @param string $value
     *
     * @return DateTime
     */
    public static function timestamp($value)
    {
        return Carbon::createFromFormat(Carbon::ATOM, $value);
    }
}