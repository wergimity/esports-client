<?php

namespace ESportsClient\Result;

use DateTime;
use ESportsClient\Helpers;
use stdClass;

/**
 * Class Player
 * @package ESportsClient\Result
 */
class Player
{
    /**
     * @var int
     */
    public $id;
    /**
     * @var string
     */
    public $name;
    /**
     * @var string
     */
    public $url;
    /**
     * @var DateTime
     */
    public $createdAt;
    /**
     * @var Team
     */
    public $team;
    /**
     * @var PlayerStatistics
     */
    public $statistics;

    /**
     * Player constructor.
     * @param stdClass $model
     */
    public function __construct(stdClass $model)
    {
        $this->id = isset($model->id) ? $model->id : null;
        $this->name = isset($model->name) ? $model->name : null;
        $this->url = isset($model->url) ? $model->url : null;
        $this->createdAt = isset($model->created_at) ? Helpers::timestamp($model->created_at) : null;
        $this->team = isset($model->team) ? new Team($model->team) : null;
        $this->statistics = isset($model->statistics) ? new PlayerStatistics($model->statistics) : null;
    }
}