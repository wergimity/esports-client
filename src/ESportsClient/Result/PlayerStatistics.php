<?php

namespace ESportsClient\Result;

use stdClass;

/**
 * Class PlayerStatistics
 * @package ESportsClient\Result
 */
class PlayerStatistics
{
    /**
     * @var int
     */
    public $id;
    /**
     * @var int
     */
    public $kills;
    /**
     * @var int
     */
    public $deaths;
    /**
     * @var Tournament
     */
    public $tournament;

    /**
     * PlayerStatistics constructor.
     * @param stdClass $model
     */
    public function __construct(stdClass $model)
    {
        $this->id = isset($model->id) ? $model->id : null;
        $this->kills = isset($model->kills) ? $model->kills : null;
        $this->deaths = isset($model->deaths) ? $model->deaths : null;
        $this->tournament = isset($model->tournament) ? new Tournament($model->tournament) : null;
    }
}