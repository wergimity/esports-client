<?php

namespace ESportsClient\Result;

use DateTime;
use ESportsClient\Helpers;
use stdClass;

/**
 * Class Team
 * @package ESportsClient\Result
 */
class Team
{
    /**
     * @var int
     */
    public $id;
    /**
     * @var string
     */
    public $name;
    /**
     * @var string
     */
    public $url;
    /**
     * @var DateTime
     */
    public $createdAt;
    /**
     * @var Player[]
     */
    public $players;
    /**
     * @var Tournament[]
     */
    public $tournaments;

    /**
     * Team constructor.
     * @param stdClass $model
     */
    public function __construct(stdClass $model)
    {
        $this->id = isset($model->id) ? $model->id : null;
        $this->name = isset($model->name) ? $model->name : null;
        $this->url = isset($model->url) ? $model->url : null;
        $this->createdAt = isset($model->created_at) ? Helpers::timestamp($model->created_at) : null;

        if (isset($model->players)) {
            $this->players = [];
            foreach ((array) $model->players as $player) {
                $this->players[] = new Player($player);
            }
        }

        if (isset($model->tournaments)) {
            $this->tournaments = [];
            foreach ($model->tournaments as $tournament) {
                $this->tournaments[] = new Tournament($tournament);
            }
        }
    }
}