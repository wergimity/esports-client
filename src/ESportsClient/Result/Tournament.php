<?php

namespace ESportsClient\Result;

use ESportsClient\Helpers;
use stdClass;

/**
 * Class Tournament
 * @package ESportsClient\Result
 */
class Tournament
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string|null
     */
    public $url;

    /**
     * @var \DateTime|null
     */
    public $startsAt;

    /**
     * @var \DateTime|null
     */
    public $endsAt;

    /**
     * @var Team[]
     */
    public $teams;

    /**
     * Tournament constructor.
     * @param stdClass $model
     */
    public function __construct(stdClass $model)
    {
        $this->id = isset($model->id) ? $model->id : null;
        $this->name = isset($model->name) ? $model->name : null;
        $this->url = isset($model->url) ? $model->url : null;
        $this->startsAt = isset($model->starts_at) ? Helpers::timestamp($model->starts_at) : null;
        $this->endsAt = isset($model->ends_at) ? Helpers::timestamp($model->starts_at) : null;
        if (isset($model->teams)) {
            $this->teams = [];
            foreach ((array) $model->teams as $team) {
                $this->teams[] = new Team($team);
            }
        }
    }
}