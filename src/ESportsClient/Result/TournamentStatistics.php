<?php

namespace ESportsClient\Result;

use stdClass;

/**
 * Class TournamentStatistics
 * @package ESportsClient\Result
 */
class TournamentStatistics
{
    /**
     * @var int
     */
    public $id;
    /**
     * @var int
     */
    public $kills;
    /**
     * @var int
     */
    public $deaths;
    /**
     * @var Player
     */
    public $player;

    /**
     * TournamentStatistics constructor.
     * @param stdClass $model
     */
    public function __construct(stdClass $model)
    {
        $this->id = isset($model->id) ? $model->id : null;
        $this->kills = isset($model->kills) ? $model->kills : null;
        $this->deaths = isset($model->deaths) ? $model->deaths : null;
        $this->player = isset($model->player) ? new Player($model->player) : null;
    }
}