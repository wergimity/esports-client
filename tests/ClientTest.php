<?php
/**
 * Created by PhpStorm.
 * User: rytisr
 * Date: 17.5.4
 * Time: 16.27
 */

use ESportsClient\Client;
use ESportsClient\Result\Player;
use ESportsClient\Result\PlayerStatistics;
use ESportsClient\Result\Team;
use ESportsClient\Result\Tournament;
use ESportsClient\Result\TournamentStatistics;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;

class ClientTest extends TestCase
{
    public function testTournamentSuccess()
    {
        $client = $this->createClientWithResponse(200);

        $this->assertInstanceOf(Tournament::class, $client->tournament(1));
    }

    public function testTournamentFailure()
    {
        $client = $this->createClientWithResponse(404);

        $this->assertEquals(null, $client->tournament(1));
    }

    public function testTournamentListSuccess()
    {
        $client = $this->createClientWithResponse(200, '[{}]');

        $this->assertInstanceOf(Tournament::class, $client->tournamentList()[0]);
    }

    public function testTournamentListFailure()
    {
        $client = $this->createClientWithResponse(404);

        $this->assertEmpty($client->tournamentList());
    }

    public function testTeamSuccess()
    {
        $client = $this->createClientWithResponse(200, '{}');

        $this->assertInstanceOf(Team::class, $client->team(1));
    }

    public function testTeamFailure()
    {
        $client = $this->createClientWithResponse(404);

        $this->assertEquals(null, $client->team(0));
    }

    public function testTeamListSuccess()
    {
        $client = $this->createClientWithResponse(200, '[{}]');

        $this->assertInstanceOf(Team::class, $client->teamList()[0]);
    }

    public function testTeamListFailure()
    {
        $client = $this->createClientWithResponse(404);

        $this->assertEmpty($client->teamList());
    }

    public function testTournamentTeamListSuccess()
    {
        $client = $this->createClientWithResponse(200, '[{}]');

        $this->assertInstanceOf(Team::class, $client->tournamentTeamList(1)[0]);
    }

    public function testTournamentTeamListFailure()
    {
        $client = $this->createClientWithResponse(404);

        $this->assertEmpty($client->tournamentTeamList(1));
    }

    public function testPlayerSuccess()
    {
        $client = $this->createClientWithResponse(200, '{}');

        $this->assertInstanceOf(Player::class, $client->player(1));
    }

    public function testPlayerFailure()
    {
        $client = $this->createClientWithResponse(404);

        $this->assertEquals(null, $client->player(0));
    }

    public function testPlayerListSuccess()
    {
        $client = $this->createClientWithResponse(200, '[{}]');

        $this->assertInstanceOf(Player::class, $client->playerList()[0]);
    }

    public function testPlayerListFailure()
    {
        $client = $this->createClientWithResponse(404);

        $this->assertEmpty($client->playerList());
    }

    public function testTeamPlayerListSuccess()
    {
        $client = $this->createClientWithResponse(200, '[{}]');

        $this->assertInstanceOf(Player::class, $client->teamPlayerList(1)[0]);
    }

    public function testTeamPlayerListFailure()
    {
        $client = $this->createClientWithResponse(404);

        $this->assertEmpty($client->teamPlayerList(1));
    }

    public function testTournamentStatisticsListSuccess()
    {
        $client = $this->createClientWithResponse(200, '[{}]');

        $this->assertInstanceOf(TournamentStatistics::class, $client->tournamentStatisticsList(1)[0]);
    }

    public function testTournamentStatisticsListFailure()
    {
        $client = $this->createClientWithResponse(404);

        $this->assertEmpty($client->tournamentStatisticsList(1));
    }

    public function testTournamentPlayerStatisticsSuccess()
    {
        $client = $this->createClientWithResponse(200, '{}');

        $this->assertInstanceOf(PlayerStatistics::class, $client->tournamentPlayerStatistics(1, 1));
    }

    public function testTournamentPlayerStatisticsFailure()
    {
        $client = $this->createClientWithResponse(404);

        $this->assertEquals(null, $client->tournamentPlayerStatistics(1, 1));
    }

    public function testClientSetterAndGetter()
    {
        $client = new Client();
        $guzzle = new \GuzzleHttp\Client();

        $this->assertNull($client->getClient());

        $client->setClient($guzzle);

        $this->assertSame($guzzle, $client->getClient());
    }

    public function testUnsetGuzzleException()
    {
        $client = new Client();

        $this->expectException(RuntimeException::class);

        $client->teamList();
    }

    private function createClientWithResponse($status, $body = '{}')
    {
        $mock = HandlerStack::create(new MockHandler([new Response($status, [], $body)]));

        return new Client(new \GuzzleHttp\Client(['handler' => $mock, 'http_errors' => false]));
    }
}
