<?php

use PHPUnit\Framework\TestCase;

class HelpersTest extends TestCase
{
    public function testTimestampSuccess()
    {
        $result = \ESportsClient\Helpers::timestamp('2017-01-01T00:00:01+0000');

        $this->assertInstanceOf(DateTime::class, $result);
    }
}