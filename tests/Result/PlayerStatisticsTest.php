<?php

use ESportsClient\Result\PlayerStatistics;
use ESportsClient\Result\Tournament;
use PHPUnit\Framework\TestCase;

class PlayerStatisticsTest extends TestCase
{
    public function testEmptyStatistics()
    {
        $stats = new PlayerStatistics((object) []);

        $this->assertNull($stats->id);
        $this->assertNull($stats->kills);
        $this->assertNull($stats->deaths);
        $this->assertNull($stats->tournament);
    }

    public function testFullStatistics()
    {
        $model = (object) [
            'id' => 1,
            'kills' => 5,
            'deaths' => 1,
            'tournament' => (object) [],
        ];

        $stats = new PlayerStatistics($model);

        $this->assertEquals($model->id, $stats->id);
        $this->assertEquals($model->kills, $stats->kills);
        $this->assertEquals($model->deaths, $stats->deaths);
        $this->assertInstanceOf(Tournament::class, $stats->tournament);
    }
}