<?php

use ESportsClient\Result\Player;
use ESportsClient\Result\PlayerStatistics;
use ESportsClient\Result\Team;
use PHPUnit\Framework\TestCase;

class PlayerTest extends TestCase
{
    public function testEmptyPlayer()
    {
        $player = new Player((object) []);

        $this->assertNull($player->id);
        $this->assertNull($player->name);
        $this->assertNull($player->url);
        $this->assertNull($player->createdAt);
        $this->assertNull($player->team);
        $this->assertNull($player->statistics);
    }

    public function testFullPlayer()
    {
        $model = (object) [
            'id' => 1,
            'name' => 'player',
            'url' => 'http://somewhere.com',
            'created_at' => '2017-01-01T00:01:00+0000',
            'team' => (object) [],
            'statistics' => (object) [],
        ];

        $player = new Player($model);

        $this->assertEquals($model->id, $player->id);
        $this->assertEquals($model->name, $player->name);
        $this->assertEquals($model->url, $player->url);
        $this->assertInstanceOf(DateTime::class, $player->createdAt);
        $this->assertInstanceOf(Team::class, $player->team);
        $this->assertInstanceOf(PlayerStatistics::class, $player->statistics);
    }
}