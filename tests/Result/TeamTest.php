<?php

use ESportsClient\Result\Player;
use ESportsClient\Result\Team;
use ESportsClient\Result\Tournament;
use PHPUnit\Framework\TestCase;

class TeamTest extends TestCase
{
    public function testEmptyTeam()
    {
        $team = new Team((object) []);

        $this->assertNull($team->id);
        $this->assertNull($team->name);
        $this->assertNull($team->url);
        $this->assertNull($team->createdAt);
        $this->assertNull($team->tournaments);
        $this->assertNull($team->players);
    }

    public function testFullTeam()
    {
        $model = (object) [
            'id' => 1,
            'name' => 'the team',
            'url' => 'http://somewhere.com',
            'created_at' => '2017-01-01T00:00:00+0000',
            'players' => [(object) []],
            'tournaments' => [(object) []],
        ];

        $team = new Team($model);

        $this->assertEquals($model->id, $team->id);
        $this->assertEquals($model->name, $team->name);
        $this->assertEquals($model->url, $team->url);
        $this->assertInstanceOf(DateTime::class, $team->createdAt);
        $this->assertInstanceOf(Player::class, $team->players[0]);
        $this->assertInstanceOf(Tournament::class, $team->tournaments[0]);
    }
}