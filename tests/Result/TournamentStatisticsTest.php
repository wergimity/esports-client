<?php

use ESportsClient\Result\Player;
use ESportsClient\Result\TournamentStatistics;
use PHPUnit\Framework\TestCase;

class TournamentStatisticsTest extends TestCase
{
    public function testEmptyStatistics()
    {
        $stats = new TournamentStatistics((object) []);

        $this->assertNull($stats->id);
        $this->assertNull($stats->kills);
        $this->assertNull($stats->deaths);
        $this->assertNull($stats->player);
    }

    public function testFullStatistics()
    {
        $model = (object) [
            'id' => 1,
            'kills' => 5,
            'deaths' => 1,
            'player' => (object) [],
        ];

        $stats = new TournamentStatistics($model);

        $this->assertEquals($model->id, $stats->id);
        $this->assertEquals($model->kills, $stats->kills);
        $this->assertEquals($model->deaths, $stats->deaths);
        $this->assertInstanceOf(Player::class, $stats->player);
    }
}