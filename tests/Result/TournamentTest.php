<?php

use ESportsClient\Result\Team;
use ESportsClient\Result\Tournament;
use PHPUnit\Framework\TestCase;

class TournamentTest extends TestCase
{
    public function testEmptyTournament()
    {
        $tournament = new Tournament((object) []);

        $this->assertNull($tournament->id);
        $this->assertNull($tournament->name);
        $this->assertNull($tournament->url);
        $this->assertNull($tournament->startsAt);
        $this->assertNull($tournament->endsAt);
        $this->assertNull($tournament->teams);
    }

    public function testFullTournament()
    {
        $model = (object) [
            'id' => 1,
            'name' => 'the tournament',
            'url' => 'http://somewhere.com',
            'starts_at' => '2017-01-01T00:00:00+0000',
            'ends_at' => '2017-01-02T00:00:00+0000',
            'teams' => [(object) []],
        ];

        $tournament = new Tournament($model);

        $this->assertEquals($model->id, $tournament->id);
        $this->assertEquals($model->name, $tournament->name);
        $this->assertEquals($model->url, $tournament->url);
        $this->assertInstanceOf(DateTime::class, $tournament->startsAt);
        $this->assertInstanceOf(DateTime::class, $tournament->endsAt);
        $this->assertInstanceOf(Team::class, $tournament->teams[0]);
    }
}